package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

//Написать приложение, которое ищет все простые числа от 0 до N включительно.
//Число N должно быть задано из стандартного потока ввода.
func main() {
	var n int
	scanner := bufio.NewScanner(os.Stdin)

	fmt.Print("Старт программы для поиска простых чисел от 0 до N\n")
	fmt.Print("Введи число N: ")
	for scanner.Scan() {
		in, err := strconv.Atoi(strings.TrimSpace(scanner.Text()))
		if err != nil || scanner.Err() != nil || in <= 0 {
			fmt.Print("----Ошибка: число должно быть целым и положительным\n----Попробуй еще раз:")
			continue
		}
		n = in
		break
	}

	fmt.Printf("Простые числа от 0 до %d:\n", n)
	for i := 0; i <= n; i++ {
		if isSimple(i) {
			fmt.Printf("%d\n", i)
		}
	}
	fmt.Print("Программа завершена")
}

func isSimple(n int) bool {
	for i := 2; i < n; i++ {
		if n%i == 0 {
			return false
		}
	}
	return n > 1
}
