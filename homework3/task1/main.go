package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"regexp"
	"strconv"
	"strings"
)

//Доработать калькулятор: больше операций и валидации данных.
func main() {
	const (
		plus     = "+"
		minus    = "-"
		multiple = "*"
		divide   = "/"
		modulus  = "%"
	)
	var (
		a, b, res float64
		operation string
		errText   = "----Ошибка. Попробуй еще раз: "
	)
	scanner := bufio.NewScanner(os.Stdin)

	fmt.Printf("Старт программы-калькулятор\n")
	fmt.Print("Введи первое число: ")
	for scanner.Scan() {
		in, err := strconv.ParseFloat(strings.TrimSpace(scanner.Text()), 64)
		if err != nil || scanner.Err() != nil {
			fmt.Printf("%s", errText)
			continue
		}
		a = in
		break
	}

	fmt.Print("Введи второе число: ")
	for scanner.Scan() {
		in, err := strconv.ParseFloat(strings.TrimSpace(scanner.Text()), 64)
		if err != nil || scanner.Err() != nil {
			fmt.Printf("%s", errText)
			continue
		}
		b = in
		break
	}

	fmt.Print("Введи арифметическую операцию (+, -, *, /, %): ")
	for scanner.Scan() {
		operations := regexp.MustCompile(`(?m)[\+\-\*\/\%]`).FindAllString(scanner.Text(), -1)
		if len(operations) > 0 {
			operation = operations[0]
		} else {
			fmt.Printf(errText)
			continue
		}

		switch operation {
		case plus:
			res = a + b
		case minus:
			res = a - b
		case multiple:
			res = a * b
		case divide:
			if b == 0 {
				fmt.Printf("%s", "----Ошибка: делить на ноль нельзя\nПрограмма завершена")
				os.Exit(1)
			}
			res = a / b
		case modulus:
			res = math.Mod(a, b)
		}
		break
	}

	fmt.Printf("Результат выполнения операции: %.2f\n", res)
	fmt.Printf("Программа завершена")
}
