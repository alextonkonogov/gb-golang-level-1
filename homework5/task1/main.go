package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

//1. Напишите приложение, рекурсивно вычисляющее заданное из стандартного ввода число Фибоначчи.
func main() {
	fmt.Print("Старт программы для поиска числа Фибоначчи по порядковому N\nДля выхода введи -1\n")
	msg := "Введи число N: "
	fmt.Print(msg)

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		if scanner.Err() != nil {
			fmt.Printf("%s", "----Ошибка чтения: \nПрограмма завершена")
			os.Exit(1)
		}

		number, err := strconv.Atoi(strings.TrimSpace(scanner.Text()))
		if err != nil || number < -1 {
			fmt.Printf("%s", "----Ошибка. Попробуй еще раз: ")
			continue
		}
		if number == -1 {
			fmt.Printf("%s", "Программа завершена")
			os.Exit(1)
		}

		fmt.Printf("----порядковому числу %d соответствует число Фибоначчи %d:\n", number, fibonacci(number))
		fmt.Print(msg)
	}
}

//Работает, но после 40 с рекурсией начинаются проблемы и ждать приходится ощутимо дольше
func fibonacci(number int) int {
	if number == 0 || number == 1 {
		return number
	}

	return fibonacci(number-2) + fibonacci(number-1)
}
