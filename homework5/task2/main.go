package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

//2. Оптимизируйте приложение за счёт сохранения предыдущих результатов в мапе.
func main() {
	fmt.Print("Старт программы для поиска числа Фибоначчи по порядковому N\nДля выхода введи -1\n")
	msg := "Введи число N: "
	fmt.Print(msg)

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		if scanner.Err() != nil {
			fmt.Printf("%s", "----Ошибка чтения: \nПрограмма завершена")
			os.Exit(1)
		}

		number, err := strconv.Atoi(strings.TrimSpace(scanner.Text()))
		if err != nil || number < -1 {
			fmt.Printf("%s", "----Ошибка. Попробуй еще раз: ")
			continue
		}
		if number == -1 {
			fmt.Printf("%s", "Программа завершена")
			os.Exit(1)
		}

		//fibonacci := fibonacciWithRecursionAndMap(number)
		fibonacci := fibonacciWithLoopAndMap(number)

		fmt.Printf("----порядковому числу %d соответствует число Фибоначчи %d\n", number, fibonacci)
		fmt.Print(msg)
	}
}

//Работает быстро и без рекурсии. Код компактнее и воспринимаается легче.
func fibonacciWithLoopAndMap(number int) int {
	fibonacciMap := map[int]int{0: 0, 1: 1}
	for i := 1; i <= number; i++ {
		if _, ok := fibonacciMap[i]; !ok {
			fibonacciMap[i] = fibonacciMap[i-1] + fibonacciMap[i-2]
		}
	}
	return fibonacciMap[number]
}

//Работает также быстро, но сложно читается + две функции
func fibonacciWithRecursionAndMap(number int) int {
	fibonacciMap := map[int]int{0: 0, 1: 1}
	if value, ok := fibonacciMap[number]; ok {
		return value
	}

	return cache(number, fibonacciMap)
}

func cache(number int, fibonacciMap map[int]int) int {
	if value, ok := fibonacciMap[number]; ok {
		return value
	}

	fibonacciMap[number] = cache(number-1, fibonacciMap) + cache(number-2, fibonacciMap)
	return fibonacciMap[number]
}
