package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"

	"gitlab/alextonkonogov/gb-golang-level-1/homework10/tests_Benchmarks/fibs"
)

//2. Оптимизируйте приложение за счёт сохранения предыдущих результатов в мапе.
func main() {
	fmt.Print("Старт программы для поиска числа Фибоначчи по порядковому N\nДля выхода введи -1\n")
	msg := "Введи число N: "
	fmt.Print(msg)

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		if scanner.Err() != nil {
			fmt.Printf("%s", "----Ошибка чтения: \nПрограмма завершена")
			os.Exit(1)
		}

		number, err := strconv.Atoi(strings.TrimSpace(scanner.Text()))
		if err != nil || number < -1 {
			fmt.Printf("%s", "----Ошибка. Попробуй еще раз: ")
			continue
		}
		if number == -1 {
			fmt.Printf("%s", "Программа завершена")
			os.Exit(1)
		}

		//fibonacci := fibonacciWithRecursionAndMap(number)
		fibonacci := fibs.FibonacciWithLoopAndMap(number)

		fmt.Printf("----порядковому числу %d соответствует число Фибоначчи %d\n", number, fibonacci)
		fmt.Print(msg)
	}
}
