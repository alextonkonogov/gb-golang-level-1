package fibs_test

import (
	"testing"

	"gitlab/alextonkonogov/gb-golang-level-1/homework10/tests_Benchmarks/fibs"
)

var number = 43

func BenchmarkFibonacciWithRecursion(b *testing.B) {
	for i := 0; i < b.N; i++ {
		fibs.FibonacciWithRecursion(number)
	}
}

func BenchmarkFibonacciWithRecursionAndMap(b *testing.B) {
	for i := 0; i < b.N; i++ {
		fibs.FibonacciWithRecursionAndMap(number)
	}
}

func BenchmarkFibonacciWithLoopAndMap(b *testing.B) {
	for i := 0; i < b.N; i++ {
		fibs.FibonacciWithLoopAndMap(number)
	}
}
