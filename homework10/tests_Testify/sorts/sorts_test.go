package sorts_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab/alextonkonogov/gb-golang-level-1/homework10/tests_Testify/sorts"
)

func TestInsertionSortTestify(t *testing.T) {
	arr := []float64{5, 1, -3, 0, 112, 45, 2, 64, 355, 12}
	sortedArr := []float64{-3, 0, 1, 2, 5, 12, 45, 64, 112, 355}

	sorts.InsertionSort(arr)
	assert.Equal(t, arr, sortedArr, "they should be equal")
}

//Очень крутая библиотека. Тест оформляется буквально в одну строчку, а внутри уже предусмотрена вся работа в плане описания входных и выходных параметров при завале теста.
//Если нажать на <Click to see difference>, то в отдельном окне IDE показывает где именно расхождения.
//Библиотека позволит сэкономить кучу времени на описании при тестировании и у разработчика будет больше возможностей заниматься именно созданием программ

//=== RUN   TestInsertionSortTestify
//sorts_test.go:32:
//Error Trace:	sorts_test.go:32
//Error:      	Not equal:
//expected: []float64{-3, 0, 1, 2, 5, 12, 45, 64, 112, 355}
//actual  : []float64{-3, 0, 1, 2, 5, 12, 45, 64, 112, 3555}
//
//Diff:
//--- Expected
//+++ Actual
//@@ -10,3 +10,3 @@
//(float64) 112,
//- (float64) 355
//+ (float64) 3555
//}
//Test:       	TestInsertionSortTestify
//Messages:   	they should be equal
//--- FAIL: TestInsertionSortTestify (0.00s)
//
//
//Expected :[]float64{-3, 0, 1, 2, 5, 12, 45, 64, 112, 355}
//Actual   :[]float64{-3, 0, 1, 2, 5, 12, 45, 64, 112, 3555}
//<Click to see difference>
//
//
//FAIL
