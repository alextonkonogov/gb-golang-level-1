package fibs

//Работает, но после 40 с рекурсией начинаются проблемы и ждать приходится ощутимо дольше
func FibonacciWithRecursion(number int) int {
	if number == 0 || number == 1 {
		return number
	}

	return FibonacciWithRecursion(number-2) + FibonacciWithRecursion(number-1)
}

//Работает быстро и без рекурсии. Код компактнее и воспринимается легче.
func FibonacciWithLoopAndMap(number int) int {
	fibonacciMap := map[int]int{0: 0, 1: 1}
	for i := 1; i <= number; i++ {
		if _, ok := fibonacciMap[i]; !ok {
			fibonacciMap[i] = fibonacciMap[i-1] + fibonacciMap[i-2]
		}
	}
	return fibonacciMap[number]
}

//Работает также быстро, но сложно читается + две функции
func FibonacciWithRecursionAndMap(number int) int {
	fibonacciMap := map[int]int{0: 0, 1: 1}
	if value, ok := fibonacciMap[number]; ok {
		return value
	}

	return cache(number, fibonacciMap)
}

func cache(number int, fibonacciMap map[int]int) int {
	if value, ok := fibonacciMap[number]; ok {
		return value
	}

	fibonacciMap[number] = cache(number-1, fibonacciMap) + cache(number-2, fibonacciMap)
	return fibonacciMap[number]
}
