package fibs_test

import (
	"testing"

	"gitlab/alextonkonogov/gb-golang-level-1/homework10/tests_TableDriven/fibs"
)

var tests = []struct {
	input int
	want  int
}{
	{input: 0, want: 0},
	{input: 1, want: 1},
	{input: 2, want: 1},
	{input: 3, want: 2},
	{input: 4, want: 3},
	{input: 5, want: 5},
	{input: 6, want: 8},
	{input: 7, want: 13},
	{input: 8, want: 21},
	{input: 9, want: 34},
	{input: 10, want: 55},
	{input: 11, want: 89},
	{input: 12, want: 144},
	{input: 13, want: 233},
	{input: 14, want: 377},
	{input: 15, want: 610},
	{input: 16, want: 987},
	{input: 17, want: 1597},
	{input: 18, want: 2584},
	{input: 19, want: 4181},
	{input: 20, want: 6765},
}

func TestFibonacciWithLoopAndMap(t *testing.T) {
	for i, tt := range tests {
		if got := fibs.FibonacciWithLoopAndMap(tt.input); got != tt.want {
			t.Errorf("on index %v got %v want %v", i, got, tt.want)
		}
	}
}
