package simples_test

import (
	"fmt"

	"gitlab/alextonkonogov/gb-golang-level-1/homework10/tests_Examples/simples"
)

func ExampleIsSimple() {
	fmt.Println(simples.IsSimple(2))
	fmt.Println(simples.IsSimple(4))
	fmt.Println(simples.IsSimple(11))
	// Output:
	//true
	//false
	//true
}
