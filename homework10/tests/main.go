package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"

	"gitlab/alextonkonogov/gb-golang-level-1/homework10/tests/sorts"
)

//1. Познакомьтесь с алгоритмом сортировки вставками.
//Напишите приложение, которое принимает на вход набор целых чисел и отдаёт на выходе его же в отсортированном виде.
//Сохраните код, он понадобится нам в дальнейших уроках.
func main() {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Printf("Старт программы сортировки\n")
	fmt.Print("Введи числа через пробел: ")
	arr := []float64{}

	for scanner.Scan() {
		if scanner.Err() != nil {
			fmt.Printf("%s", "----Ошибка чтения: \nПрограмма завершена")
			os.Exit(1)
		}
		numsTxt := strings.Split(scanner.Text(), " ")
		for _, numTxt := range numsTxt {
			num, err := strconv.ParseFloat(strings.TrimSpace(numTxt), 64)
			if err != nil {
				//игнорируем все лишнее
				continue
			}
			arr = append(arr, num)
		}
		break
	}

	fmt.Printf("Результат до сортировки: %v\n", arr)
	sorts.InsertionSort(arr)
	fmt.Printf("Результат после сортировки: %v\n", arr)
	fmt.Printf("Программа завершена")
}
