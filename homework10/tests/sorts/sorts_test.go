package sorts_test

import (
	"testing"

	"gitlab/alextonkonogov/gb-golang-level-1/homework10/tests/sorts"
)

func TestInsertionSort(t *testing.T) {
	arr := []float64{5, 1, -3, 0, 112, 45, 2, 64, 355, 12}
	sortedArr := []float64{-3, 0, 1, 2, 5, 12, 45, 64, 112, 355}

	sorts.InsertionSort(arr)
	if len(arr) != len(sortedArr) {
		t.Errorf("Expected %v got %v", sortedArr, arr)
		return
	}
	for i, v := range arr {
		if v != sortedArr[i] {
			t.Errorf("Failed on index %v. Expected %v got %v ", i, sortedArr, arr)
		}
	}
}
