package calculation

import (
	"errors"
	"math"
)

const (
	plus     = "+"
	minus    = "-"
	multiple = "*"
	divide   = "/"
	modulus  = "%"
)

var validOperations = map[string]struct{}{
	plus:     struct{}{},
	minus:    struct{}{},
	multiple: struct{}{},
	divide:   struct{}{},
	modulus:  struct{}{},
}

type сalculation struct {
	a, b, res float64
	operation string
}

func (c *сalculation) SetA(value float64) {
	c.a = value
}

func (c *сalculation) SetB(value float64) {
	c.b = value
}

func (c *сalculation) SetOperation(value string) error {
	if _, ok := validOperations[value]; !ok {
		return errors.New("тип операции не поддерживается")
	}
	c.operation = value
	return nil
}

func (c *сalculation) MakeCalculation() (err error) {
	switch c.operation {
	case plus:
		c.res = c.a + c.b
	case minus:
		c.res = c.a - c.b
	case multiple:
		c.res = c.a * c.b
	case divide:
		if c.b == 0 {
			return errors.New("----ошибка: делить на ноль нельзя")
		}
		c.res = c.a / c.b
	case modulus:
		c.res = math.Mod(c.a, c.b)
	default:
		err = errors.New("Тип операции не поддерживается")
	}
	return err
}

func (c *сalculation) GetResult() float64 {
	return c.res
}

func NewCalculation() *сalculation {
	return &сalculation{}
}
