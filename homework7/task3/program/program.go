package program

import (
	"bufio"
	"fmt"
	"strconv"
	"strings"
)

const (
	A = "A"
	B = "B"
)

type Calculator interface {
	SetA(float64)
	SetB(float64)
	SetOperation(string) error
	MakeCalculation() error
	GetResult() float64
}

type programCalculator struct {
	calculator Calculator
	scanner    *bufio.Scanner
}

func (pc *programCalculator) Start() (err error) {
	err = pc.setOperand("Введи первое число: ", A)
	if err != nil {
		return
	}
	err = pc.setOperand("Введи второе число: ", B)
	if err != nil {
		return
	}
	err = pc.setOperation("Введи арифметическую операцию (+, -, *, /, %): ")
	if err != nil {
		return
	}

	err = pc.calculate()
	if err != nil {
		return
	}

	fmt.Printf("Результат выполнения операции: %.2f\n\n", pc.calculator.GetResult())
	return nil
}

func (pc *programCalculator) setOperand(msg string, operand string) error {
	fmt.Print(msg)
	for pc.scanner.Scan() {
		if pc.scanner.Err() != nil {
			return fmt.Errorf("----Ошибка чтения. Программа завершена")
		}
		in, err := strconv.ParseFloat(strings.TrimSpace(pc.scanner.Text()), 64)
		if err != nil {
			fmt.Printf("%s", "----попробуй еще раз: ")
			continue
		}

		switch operand {
		case A:
			pc.calculator.SetA(in)
		case B:
			pc.calculator.SetB(in)
		}
		break
	}
	return nil
}

func (pc *programCalculator) setOperation(msg string) error {
	fmt.Print(msg)
	for pc.scanner.Scan() {
		err := pc.calculator.SetOperation(strings.TrimSpace(pc.scanner.Text()))
		if err != nil {
			fmt.Printf("%s", "----попробуй еще раз: ")
			continue
		}
		break
	}
	return nil
}

func (pc *programCalculator) calculate() error {
	return pc.calculator.MakeCalculation()
}

func NewProgram(calculator Calculator, scanner *bufio.Scanner) *programCalculator {
	return &programCalculator{calculator: calculator, scanner: scanner}
}
