package main

import (
	"bufio"
	"fmt"
	"os"

	"gitlab/alextonkonogov/gb-golang-level-1/homework7/task3/calculation"
	"gitlab/alextonkonogov/gb-golang-level-1/homework7/task3/program"
)

//Переписать калькулятор с использованием интерфейсов

//Чтобы применить в тему интерфейсы я вынес логику самой программы в отдельный пакет "program"
//в пакете "program" я использую возможности пакета "calculation", но взаимодействую с ним через интерфейс
//и это позволяет мне обойтись без явного импорта пакета "calculation" в "program"
func main() {
	programCalculator := program.NewProgram(calculation.NewCalculation(), bufio.NewScanner(os.Stdin))
	for {
		err := programCalculator.Start()
		if err != nil {
			fmt.Printf("%s\n\n", err.Error())
			continue
		}
	}
}
