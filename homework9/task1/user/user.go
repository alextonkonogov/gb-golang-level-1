package user

import (
	"fmt"
	"net/url"
)

type user struct {
	Name       string `json:"name"`
	City       string `json:"city"`
	Age        int    `json:"age"`
	SocialLink string `json:"social_link"`
}

func (u *user) Check() (err error) {
	if u.Age > 120 {
		err = fmt.Errorf("unlikely you are older 120 years")
		return
	} else if u.Age < 5 {
		err = fmt.Errorf("unlikely you are yonger 5 years")
		return
	}

	_, err = url.ParseRequestURI(u.SocialLink)
	if err != nil {
		err = fmt.Errorf("incorrect URL")
	}
	return
}

func (u *user) GetHello() string {
	return fmt.Sprintf(
		"<div><h4>Hello from %s</h1><p>I am %v years old and live in %s</p><p>Follow me on <a href=\"%s\" target=\"_blank\">my social network page</a></p></div>",
		u.Name, u.Age, u.City, u.SocialLink,
	)
}

func NewUser() *user {
	return &user{}
}
