package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"
)

type App struct {
	Name string `json:"name"`
	Port int    `json:"port"`
}

func (a *App) Check() error {
	if a.Port < 1000 || a.Port > 9999 {
		return fmt.Errorf("invalid port")
	}

	if strings.TrimSpace(a.Name) == "" {
		return fmt.Errorf("your app name cant be empty")
	}

	return nil
}

type Helloer interface {
	Check() error
	GetHello() string
}

func ReadConfFromFile(filename string) (app *App, err error) {
	filedata, err := ioutil.ReadFile(filename)
	if err != nil {
		err = fmt.Errorf("cant read file %v: %v", filename, err.Error())
		return
	}

	err = json.Unmarshal(filedata, &app)
	if err != nil {
		err = fmt.Errorf("cant unmarshal from file %v: %v", filename, err.Error())
		return
	}

	return app, app.Check()
}

func ReadUserDataFromFile(filename string, h Helloer) (Helloer, error) {
	filedata, err := ioutil.ReadFile(filename)
	if err != nil {
		err = fmt.Errorf("cant read file %v: %v", filename, err.Error())
		return nil, err
	}

	err = json.Unmarshal(filedata, &h)
	if err != nil {
		err = fmt.Errorf("cant unmarshal from file %v: %v", filename, err.Error())
		return nil, err
	}

	return h, h.Check()
}
