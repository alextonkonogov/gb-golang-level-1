package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	"gitlab/alextonkonogov/gb-golang-level-1/homework9/task1/config"
	"gitlab/alextonkonogov/gb-golang-level-1/homework9/task1/user"
)

//Разработайте пакет для чтения конфигурации типичного веб-приложения через флаги или переменные окружения.
//Помимо чтения конфигурации приложение также должно валидировать её, например все URL’ы должны соответствовать ожидаемым форматам.
//Работу с конфигурацией необходимо вынести в отдельный пакет (не в пакет main).
var (
	configfile = flag.String("configfile", "config.json", "Имя файла с настройками в формате json")
	userfile   = flag.String("userfile", "elon.json", "Имя файла c данными пользователя в формате json")
)

func main() {
	flag.Parse()
	cnfg, err := config.ReadConfFromFile(*configfile)
	if err != nil {
		log.Fatal(err)
	}

	u, err := config.ReadUserDataFromFile(*userfile, user.NewUser())
	if err != nil {
		log.Fatal(err)
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		fmt.Fprint(w, u.GetHello())
	})

	fmt.Printf("%s is working\nClick http://localhost:%v/ to open", cnfg.Name, cnfg.Port)
	http.ListenAndServe(fmt.Sprintf(":%v", cnfg.Port), nil)
}
