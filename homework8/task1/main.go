package main

import (
	"encoding/json"
	"fmt"
	"log"

	"gitlab/alextonkonogov/gb-golang-level-1/homework8/task1/cnfg"
)

//Разработайте пакет для чтения конфигурации типичного веб-приложения через флаги или переменные окружения.
//Помимо чтения конфигурации приложение также должно валидировать её, например все URL’ы должны соответствовать ожидаемым форматам.
//Работу с конфигурацией необходимо вынести в отдельный пакет (не в пакет main).
func main() {
	cnfg, err := cnfg.NewAppConfig()
	if err != nil {
		log.Fatal(err)
	}

	jsonBts, err := json.Marshal(cnfg)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Конфиг приложения в JSON:\n%s\n", string(jsonBts))
}
