package cnfg

import (
	"flag"
	"fmt"
	"net/url"
)

var (
	name    = flag.String("name", "JSON config printer", "Название вашего приложения")
	port    = flag.Int("port", 5555, "Порт вашего приложения")
	timeout = flag.Float64("timeout", 5.0, "Максимальный таймаут")
	link    = flag.String("link", "https://gb.ru", "Какая-то ссылка")
)

type appConfig struct {
	Name    string  `json:"name"`
	Port    int     `json:"port"`
	Timeout float64 `json:"timeout"`
	Link    string  `json:"link"`
}

func (c *appConfig) Check() error {
	if c.Port < 1000 || c.Port > 9999 {
		return fmt.Errorf("Невалидный порт")
	}
	if c.Timeout > 10 {
		return fmt.Errorf("Таймаут не может быть больше 10 секунд")

	} else if c.Timeout < 0.5 {
		return fmt.Errorf("Таймаут не может быть меньше 0.5 секунды")
	}

	_, err := url.ParseRequestURI(c.Link)
	if err != nil {
		return fmt.Errorf("Неверный URL %v", err)
	}

	return nil
}

func NewAppConfig() (*appConfig, error) {
	flag.Parse()
	config := &appConfig{*name, *port, *timeout, *link}
	return config, config.Check()
}
