package main

import (
	"bufio"
	"fmt"
	calc "gitlab/alextonkonogov/gb-golang-level-1/homework6/task3/calculation"
	"os"
	"strconv"
	"strings"
)

//Переписать калькулятор с использованием структур, методов и указателей
func main() {
	calculation := new(calc.Calculation)
	var errText = "----Ошибка. Попробуй еще раз: "

	scanner := bufio.NewScanner(os.Stdin)

	fmt.Printf("Старт программы-калькулятор\n")
	fmt.Print("Введи первое число: ")
	for scanner.Scan() {
		if scanner.Err() != nil {
			fmt.Printf("%s", "----Ошибка чтения. Программа завершена")
			os.Exit(1)
		}
		in, err := strconv.ParseFloat(strings.TrimSpace(scanner.Text()), 64)
		if err != nil {
			fmt.Printf("%s", errText)
			continue
		}
		calculation.SetA(in)
		break
	}

	fmt.Print("Введи второе число: ")
	for scanner.Scan() {
		if scanner.Err() != nil {
			fmt.Printf("%s", "----Ошибка чтения. Программа завершена")
			os.Exit(1)
		}
		in, err := strconv.ParseFloat(strings.TrimSpace(scanner.Text()), 64)
		if err != nil {
			fmt.Printf("%s", errText)
			continue
		}
		calculation.SetB(in)
		break
	}

	fmt.Print("Введи арифметическую операцию (+, -, *, /, %): ")
	for scanner.Scan() {
		err := calculation.MakeOperation(strings.TrimSpace(scanner.Text()))
		if err != nil {
			fmt.Printf("%s", err.Error())
			os.Exit(1)
		}
		break
	}

	fmt.Printf("Результат выполнения операции: %.2f\n", calculation.GetResult())
	fmt.Printf("Программа завершена")
}
