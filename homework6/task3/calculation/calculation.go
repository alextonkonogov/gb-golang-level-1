package calculation

import (
	"errors"
	"math"
)

const (
	plus     = "+"
	minus    = "-"
	multiple = "*"
	divide   = "/"
	modulus  = "%"
)

type Calculation struct {
	a, b, res float64
	operation string
}

func (c *Calculation) SetA(value float64) {
	c.a = value
}

func (c *Calculation) SetB(value float64) {
	c.b = value
}

func (c *Calculation) MakeOperation(operation string) (err error) {
	switch operation {
	case plus:
		c.plus()
	case minus:
		c.minus()
	case multiple:
		c.multiple()
	case divide:
		err = c.divide()
	case modulus:
		c.modulus()
	default:
		err = errors.New("Тип операции не поддерживается")
	}
	return err
}

func (c *Calculation) GetResult() float64 {
	return c.res
}

func (c *Calculation) plus() {
	c.res = c.a + c.b
}

func (c *Calculation) minus() {
	c.res = c.a - c.b
}

func (c *Calculation) multiple() {
	c.res = c.a * c.b
}

func (c *Calculation) divide() error {
	if c.b == 0 {
		return errors.New("----Ошибка: делить на ноль нельзя\nПрограмма завершена")
	}
	c.res = c.a / c.b
	return nil
}

func (c *Calculation) modulus() {
	c.res = math.Mod(c.a, c.b)
}
