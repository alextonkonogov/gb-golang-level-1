package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

//Напишите программу для вычисления площади прямоугольника. Длины сторон прямоугольника должны вводиться пользователем с клавиатуры.
func main() {
	var length, width int
	var errText = "----Ошибка: число должно быть целым и положительным\n----Попробуй еще раз:"
	scanner := bufio.NewScanner(os.Stdin)

	fmt.Printf("Старт программы для вычисления площади прямоугольника\n")
	fmt.Print("Введи значение длины: ")
	for scanner.Scan() {
		in, err := strconv.Atoi(scanner.Text())
		if err != nil || scanner.Err() != nil || in <= 0 {
			fmt.Printf(errText)
		} else {
			length = in
			break
		}
	}

	fmt.Print("Введи значение ширины: ")
	for scanner.Scan() {
		in, err := strconv.Atoi(scanner.Text())
		if err != nil || scanner.Err() != nil || in <= 0 {
			fmt.Printf(errText)
		} else {
			width = in
			break
		}
	}

	fmt.Printf("Площадь прямоугольника равна: %#v\n", length*width)
	fmt.Printf("Программа завершена")
}
