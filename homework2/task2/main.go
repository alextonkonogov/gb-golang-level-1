package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
)

//Напишите программу, вычисляющую диаметр и длину окружности по заданной площади круга. Площадь круга должна вводиться пользователем с клавиатуры.
func main() {
	var square, diameter, radius float64
	scanner := bufio.NewScanner(os.Stdin)

	fmt.Printf("Старт программы для вычисления диаметра и длины окружности\n")
	fmt.Print("Введи значение площади: ")
	for scanner.Scan() {
		in, err := strconv.ParseFloat(scanner.Text(), 64)
		if err != nil || scanner.Err() != nil || in <= 0 {
			fmt.Printf("----Ошибка: число должно положительным\n----Попробуй еще раз:")
		} else {
			square = in
			break
		}
	}

	radius = math.Sqrt(square / math.Pi)
	diameter = radius * 2
	fmt.Printf("Диаметр равен %.2f Радиус равен : %.2f\n", diameter, radius)
	fmt.Printf("Программа завершена")
}
