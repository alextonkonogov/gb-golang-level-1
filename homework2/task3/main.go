package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
)

//С клавиатуры вводится трехзначное число. Выведите цифры, соответствующие количество сотен, десятков и единиц в этом числе.
func main() {
	const (
		onesTrue = iota + 1
		dozensTrue
		hundredsTrue
	)

	var hundreds, dozens, ones = "0", "0", "0"
	scanner := bufio.NewScanner(os.Stdin)

	fmt.Printf("Старт программы для вычисления сотен, десятков и единиц\n")
	fmt.Print("Введи число: ")
	for scanner.Scan() {
		in, err := strconv.Atoi(scanner.Text())
		if err != nil || scanner.Err() != nil || in < 0 || in > 999 {
			fmt.Printf("----Ошибка: число должно быть целым от 0 до 999 \n----Попробуй еще раз:")
		} else {
			numbers := regexp.MustCompile(`(?m)\d`).FindAllString(scanner.Text(), -1)
			if len(numbers) == hundredsTrue {
				hundreds, dozens, ones = numbers[0], numbers[1], numbers[2]
			} else if len(numbers) == dozensTrue {
				dozens, ones = numbers[0], numbers[1]
			} else if len(numbers) == onesTrue {
				ones = numbers[0]
			}
			break
		}
	}

	fmt.Printf("Сотен %s Десятков %s Единиц %s\n", hundreds, dozens, ones)
	fmt.Printf("Программа завершена")
}
